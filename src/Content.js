const Content = [

    {
        id: 1,
        title: 'Grow Together',
        body: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius enim, quisquam vel doloremque, ipsam, repellat error fugiat provident minus facilis sit porro ducimus! Earum natus aut nemo ipsum obcaecati voluptatum.',
        image: 'illustration-grow-together.svg'
    },
    {
        id: 2,
        title: 'Flowing Conversations',
        body: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius enim, quisquam vel doloremque, ipsam, repellat error fugiat provident minus facilis sit porro ducimus! Earum natus aut nemo ipsum obcaecati voluptatum.',
        image: 'illustration-flowing-conversation.svg'
    },
    {
        id: 3,
        title: 'Grow users',
        body: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius enim, quisquam vel doloremque, ipsam, repellat error fugiat provident minus facilis sit porro ducimus! Earum natus aut nemo ipsum obcaecati voluptatum.',
        image: 'illustration-your-users.svg'
    }
]


export default Content