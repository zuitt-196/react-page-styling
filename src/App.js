
import { ThemeProvider } from 'styled-components';
import Header from './components/Header';
import Footer from './components/Footer';
import Card from './components/Card';
import { Container } from './components/styles/Container.styled';
import GlobalStyle from './components/styles/Global';

import Content from './Content'



const theme = {
  colors: {
    header: '#ebfbff',
    body: '#fff',
    footer: '#0b7d59'
  },

  mobile: '786px'



}

function App() {
  return (
    < ThemeProvider theme={theme}>
      <>
        <GlobalStyle />
        <Header />
        <Container>
          {Content.map((item, index) => (
            <Card key={index} item={item} />

          ))}
        </Container>
        <Footer />
      </>
    </ThemeProvider >

  )
}

export default App;
