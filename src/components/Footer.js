import React from 'react'
import { Container } from './styles/Container.styled';
import { Flex } from './styles/Flex.styled';
import { StyledFooter } from "./styles/Footer.styled";
import SocialIcon from "./SocialIcon"

const Footer = () => {
    return (
        <StyledFooter>
            <Container>
                <img src="./images/logo_white.svg" alt="" />
                <Flex>
                    <ul>
                        <li>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos, deserunt?
                        </li>
                        <li>+0-0910-27-64-396</li>
                        <li><a href="https://www.facebook.com/bongbong.bercasio.9">Master Vhong Code</a></li>
                    </ul>

                    <ul>
                        <li>Abbout Us</li>
                        <li>What we do</li>
                        <li>FAQ</li>
                    </ul>

                    <ul>
                        <li>Carrer</li>
                        <li>Blog</li>
                        <li>Contact</li>
                    </ul>

                    <SocialIcon />
                </Flex>
                <p>&copy; 2022 Mater Vhong Code. All rights reserved.</p>
            </Container>
        </StyledFooter>

    )
}

export default Footer