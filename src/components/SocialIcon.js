import React from 'react'
import { FaTwitter, FaFacebook, FaLinkedin } from 'react-icons/fa';
import { StyleSocialIcons } from './styles/SocialIcons.styled'


const SocialIcon = () => {
    return (
        <StyleSocialIcons>
            <li><a href="#"><FaTwitter /></a>
                <a href="https://www.facebook.com/bongbong.bercasio.9"><FaFacebook /></a>
                <a href="#"><FaLinkedin /></a></li>
        </StyleSocialIcons >

    )
}

export default SocialIcon