import styled from "styled-components";

export const StyleSocialIcons = styled.div`
    dislplay: flex;
    align-items: center;
    justify-content: center;
    
    li{
        list-style: none;
    }

    a {
  
        border: 1px solid #fff;
        border-radius: 50%;
        color: red;
        display: inline-flex;
        align-items: center;
        justify-content: center;
        margin-right: 10px;
        height: 40px;
        width: 40px;
        text-decoration:none;  
    }

`