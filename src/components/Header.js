import { StyledHeader, Nav, Logo , Image} from './styles/Header.styled';
// import the customize Container
import { Container } from './styles/Container.styled';
// import header

// import flex
import { Flex } from './styles/Flex.styled';


// import Button
import { Button } from './styles/Button.styled';

const Header = () => {
    return (
        <StyledHeader bg="red">
            <Container>
                <Nav>
                    <Logo src='./images/logo.svg' alt="Logo" />
                    <Button>Try IT Free</Button>
                </Nav>
                <Flex>
                    <div>
                        <h1>Build The Community Your Fans Will Love</h1>

                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore voluptatem rem numquam? Veritatis aspernatur odit unde suscipit dolores reprehenderit alias aut maxime autem consequatur. Officiis totam accusamus quam adipisci nihil?
                        </p>

                        <Button bg='#ff0099' color='#fff'>
                            Get Started For Free
                        </Button>
                    </div>

                    <Image src='./images/illustration-mockups.svg' />
                </Flex>
            </Container>
        </StyledHeader>
    )
}

export default Header